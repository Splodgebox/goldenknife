package net.aminecraftdev.goldenknife;

import net.aminecraftdev.goldenknife.commands.GoldenKnifeCmd;
import net.aminecraftdev.goldenknife.listeners.KnifeHitListener;
import net.aminecraftdev.goldenknife.utils.*;
import net.aminecraftdev.goldenknife.utils.factory.NbtFactory;
import net.aminecraftdev.goldenknife.utils.file.QFile;
import net.aminecraftdev.goldenknife.utils.file.types.YmlQFile;
import net.aminecraftdev.goldenknife.utils.itemstack.ItemStackUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

/**
 * Created by charl on 21-Apr-17.
 */
public class GoldenKnife extends JavaPlugin {

    private static final String GOLDEN_KNIFE = "GOLDEN-KNIFE", DURABILITY = "GK-DURABILITY";

    private static GoldenKnife i;

    private YmlQFile lang, config;
    private ItemStack goldenKnifeItemStack;

    @Override
    public void onEnable() {
        i = this;

        new MassiveStats(this);
        new NMSVersionHandler();
        new ServerUtils(this);
        QFile.setPlugin(this);

        loadFiles();
        loadMessages();
        reloadFiles();
        loadCommands();
        loadListeners();
    }

    public void loadMessages() {
        lang = new YmlQFile(new File(getDataFolder(), "lang.yml")).saveResource(false).createFile();

        for(Message m : Message.values()) {
            if(!lang.getConfig().contains(m.getPath())) {
                lang.getConfig().set(m.getPath(), m.getDefault());
            }
        }

        lang.saveFile();
        Message.setFile(lang.getConfig());
    }

    public void loadFiles() {
        lang = new YmlQFile(new File(getDataFolder(), "lang.yml")).saveResource(true).createFile();
        config = new YmlQFile(new File(getDataFolder(), "config.yml")).saveResource(true).createFile();
    }

    public void loadCommands() {
        new GoldenKnifeCmd();
    }

    public void loadListeners() {
        Bukkit.getPluginManager().registerEvents(new KnifeHitListener(), this);
    }

    public void saveFiles() {
        lang.saveFile();
        config.saveFile();
    }

    public void reloadFiles() {
        lang.reloadFile();
        config.reloadFile();

        StaticHandler.dropOnGround = config.getConfig().getBoolean("Settings.dropOnGroundIfInvFull", true);
        StaticHandler.instantKillChance = config.getConfig().getDouble("Settings.instantKillChance", 50.0);
        StaticHandler.failKillDamageWeapon = config.getConfig().getBoolean("Settings.FailKill.damageWeapon", false);
        StaticHandler.failKillKillAttacker = config.getConfig().getBoolean("Settings.FailKill.killAttacker", true);
        StaticHandler.goldenKnifeItem = config.getConfig().getConfigurationSection("ItemStacks.GoldenKnife");
        StaticHandler.knifeDurability = config.getConfig().getInt("Settings.knifeDurability", 2);

        updateItemStack();
    }

    private void updateItemStack() {
        ItemStack itemStack = ItemStackUtils.createItemStack(StaticHandler.goldenKnifeItem);
        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound nbtCompound = NbtFactory.fromItemTag(craftStack);

        nbtCompound.put(GOLDEN_KNIFE, "");
        nbtCompound.put(DURABILITY, 0);

        goldenKnifeItemStack = craftStack;
    }

    public ItemStack getGoldenKnife() {
        return goldenKnifeItemStack.clone();
    }

    public int getDurability(ItemStack itemStack) {
        NbtFactory.NbtCompound craftStack = NbtFactory.fromItemTag(NbtFactory.getCraftItemStack(itemStack));

        return craftStack.getInteger(DURABILITY, 0);
    }

    public ItemStack setDurability(ItemStack itemStack, int durability) {
        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        compound.put(DURABILITY, durability);
        return craftStack;
    }

    public boolean isNotGoldenKnife(ItemStack itemStack) {
        if(itemStack == null || itemStack.getType() == Material.AIR) return true;

        if(itemStack.getType() == getGoldenKnife().getType()) {
            NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(NbtFactory.getCraftItemStack(itemStack));

            return !compound.containsKey(GOLDEN_KNIFE);
        }

        return true;
    }

    public static GoldenKnife getI() {
        return i;
    }

}
