package net.aminecraftdev.goldenknife.utils;

import java.text.DecimalFormat;

/**
 * Created by charl on 28-Apr-17.
 */
public class NumberUtils {

    private static DecimalFormat numberFormat = new DecimalFormat("###,###,###,###,###.##");
    private static DecimalFormat decimalFormat = new DecimalFormat("#.##");

    public static String formatDouble(double d) {
        return numberFormat.format(d);
    }

    public static String formatDecimals(double d) {
        return decimalFormat.format(d);
    }

    public static String formatTime(int time) {
        int hours = time / 3600;
        int remainder = time % 3600;
        int minutes = remainder / 60;
        int seconds = remainder % 60;
        String disHour = (hours < 10 ? "0" : "") + hours;
        String disMinu = (minutes < 10 ? "0" : "") + minutes;
        String disSeco = (seconds < 10 ? "0" : "") + seconds;
        String formatted = "";

        if(hours != 0) formatted += disHour + " hours ";
        if(minutes != 0) formatted += disMinu + " minutes ";
        if(seconds != 0) formatted += disSeco + " seconds.";

        return formatted;
    }

    public static boolean isStringInteger(String s) {
        try {
            Integer.valueOf(s);
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }

    public static boolean isStringDouble(String s) {
        try {
            Double.valueOf(s);
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }

}