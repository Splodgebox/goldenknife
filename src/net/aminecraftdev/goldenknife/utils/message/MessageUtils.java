package net.aminecraftdev.goldenknife.utils.message;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by charl on 28-Apr-17.
 *
 */
public class MessageUtils {

    private final static int MAX_PX = 319;

    public static int getMaxPixel() {
        return MAX_PX;
    }

    public static boolean containsCenteredComponent(BaseComponent[] baseComponents) {
        for(BaseComponent baseComponent : baseComponents) {
            if(baseComponent.toPlainText().contains("{c}")) return true;
        }

        return false;
    }

    public static BaseComponent[] getCenteredComponent(BaseComponent[] baseComponents) {
        List<BaseComponent> baseComponentList = new ArrayList<>();
        int messagePxSize = 0;
        boolean cancelled = false;

        if(baseComponents == null) return null;

        for(BaseComponent baseComponent : baseComponents) {
            String plainText = baseComponent.toPlainText();

            plainText = ChatColor.stripColor(plainText);

            if(cancelled) break;
            if(plainText.equals("") || plainText.isEmpty()) continue;
            if(plainText.replace(" ", "").equals("{c}")) continue;

            if(plainText.contains("{c}")) {
                if(baseComponent instanceof TextComponent) {
                    TextComponent component = (TextComponent) baseComponent;

                    component.setText(component.getText().replace("{c}", ""));
                    plainText = component.getText();
                } else {
                    baseComponent.setInsertion(baseComponent.getInsertion().replace("{c}", ""));
                    plainText = baseComponent.getInsertion();
                }
            }

            for(char c : ChatColor.stripColor(plainText).toCharArray()) {
                DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);
                int sizeIncrease = baseComponent.isBold()? dFI.getBoldLength() : dFI.getLength();

                if((messagePxSize + sizeIncrease) > MAX_PX) {
                    cancelled = true;
                    break;
                }

                messagePxSize += sizeIncrease;
            }

            baseComponentList.add(baseComponent);
        }

        if(messagePxSize < MAX_PX) {
            int remainingPixels = MAX_PX - messagePxSize;

            if(remainingPixels != 0) {
                boolean remainingPixelsEven = (remainingPixels % 2) == 0;
                int remainingPixelsL = (remainingPixels / 2);
                int remainingPixelsR = remainingPixels / 2;

                if (!remainingPixelsEven) {
                    remainingPixelsL += (remainingPixels % 2);
                }

                int spaceLength = DefaultFontInfo.SPACE.getLength();
                BaseComponent[] returnComponents;

                if (remainingPixels >= spaceLength && remainingPixels < (spaceLength * 2)) {
                    BaseComponent baseComponent = new TextComponent(" ");

                    returnComponents = new BaseComponent[baseComponentList.size() + 1];
                    returnComponents[0] = baseComponent;

                    for(int i = 1; i <= baseComponentList.size(); i++) {
                        returnComponents[i] = baseComponentList.get(i-1);
                    }
                } else {
                    int spacesL = remainingPixelsL > 4? remainingPixelsL / spaceLength : 0;
                    int spacesR = remainingPixelsR > 4? remainingPixelsR / spaceLength : 0;
                    int slot = 0;

                    returnComponents = new BaseComponent[baseComponentList.size() + spacesL + spacesR];

                    if(spacesL > 0) {
                        for (int i = 0; i < spacesL; i++) {
                            returnComponents[slot] = new TextComponent(" ");
                            slot += 1;
                        }
                    }

                    for(int i = 0; i < baseComponentList.size(); i++) {
                        returnComponents[slot] = baseComponentList.get(i);
                        slot += 1;
                    }

                    if(spacesR > 0) {
                        for (int i = 0; i < spacesR; i++) {
                            returnComponents[slot] = new TextComponent(" ");
                            slot += 1;
                        }
                    }
                }

                return returnComponents;
            }
        }

        return baseComponents;
    }

    public static String getCenteredMessage(String message) {
        if(message == null || message.equals("")) return "";

        message = translateString(message);

        int messagePxSize = 0;
        boolean previousCode = false;
        boolean isBold = false;
        int charIndex = 0;
        int lastSpaceIndex = 0;

        for(char c : message.toCharArray()) {
            if(c == '§') {
                previousCode = true;
                continue;
            } else if(previousCode) {
                previousCode = false;

                if(c == 'l' || c == 'L') {
                    isBold = true;
                    continue;
                } else {
                    isBold = false;
                }
            } else if(c == ' ') {
                lastSpaceIndex = charIndex;
            } else {
                DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);

                messagePxSize += isBold ? dFI.getBoldLength() : dFI.getLength();
            }

            if(messagePxSize > MAX_PX) {
                if(lastSpaceIndex != 0) {
                    message = message.substring(0, lastSpaceIndex);
                } else {
                    message = message.substring(0, charIndex);
                }

                break;
            }

            charIndex++;
        }

        StringBuilder stringBuilder = new StringBuilder();

        if(messagePxSize < MAX_PX) {
            int remainingPixels = MAX_PX - messagePxSize;

            if(remainingPixels != 0) {
                boolean remainingPixelsEven = (remainingPixels % 2) == 0;
                int remainingPixelsL = (remainingPixels / 2);
                int remainingPixelsR = remainingPixels / 2;

                if(!remainingPixelsEven) {
                    remainingPixelsL += (remainingPixels % 2);
                }

                int spaceLength = DefaultFontInfo.SPACE.getLength();

                if(remainingPixels >= spaceLength && remainingPixels < (spaceLength * 2)) {
                    stringBuilder.append(" ");
                    stringBuilder.append(message);
                } else {
                    if(remainingPixelsL > 4) {
                        int spaces = remainingPixelsL / spaceLength;

                        for(int i = 1; i <= spaces; i++) {
                            stringBuilder.append(" ");
                        }
                    }

                    stringBuilder.append(message);

                    if(remainingPixelsR > 4) {
                        int spaces = remainingPixelsR / spaceLength;

                        for(int i = 1; i <= spaces; i++) {
                            stringBuilder.append(" ");
                        }
                    }
                }
            } else {
                stringBuilder.append(message);
            }
        }

        return stringBuilder.toString();
    }

    public static void sendCenteredMessage(CommandSender player, String message) {
        if(message == null || message.equals("")) player.sendMessage("");

        player.sendMessage(getCenteredMessage(message));
    }

    public static void broadcastMessage(String message) {
        Bukkit.getOnlinePlayers().forEach(player -> player.sendMessage(translateString(message)));
    }

    public static final String translateString(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

}
