package net.aminecraftdev.goldenknife.utils.command;

import net.aminecraftdev.goldenknife.utils.command.attributes.*;
import net.aminecraftdev.goldenknife.utils.message.MessageUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;

/**
 * Created by Lukas Bingham.
 *
 * All credits of this class go to Lukas, he is the creator of this class.
 */
public abstract class CommandService<T extends CommandSender> extends BukkitCommand {

    private static CommandMap _commandMap = null;

    private Class<T> parameterClass;
    private String command, description;
    private String[] aliases;
    private String suggest,  permission, noPermissionMsg;

    /**
     * Construct a new command.
     *
     * @param command     The command label
     * @param description The command description
     * @param aliases     Aliases of the command
     */
    public CommandService(String command, String description, String[] aliases) {
        super(command);

        this.command = command;
        this.description = description;
        this.aliases = aliases;
    }

    /**
     * Construct a new command.
     *
     * @param command     The command label
     * @param description The command description
     */
    public CommandService(String command, String description) {
        this(command, description, new String[]{});
    }

    /**
     * Construct a new command.
     *
     * @param cmd The command
     */
    public CommandService(Class<? extends CommandService> cmd) {
        this(cmd.getAnnotation(Name.class).value(), cmd.getAnnotation(Description.class).value());

        if(cmd.isAnnotationPresent(Alias.class))
            this.aliases = cmd.getAnnotation(Alias.class).value();

        if(cmd.isAnnotationPresent(Suggest.class))
            this.suggest = cmd.getAnnotation(Suggest.class).value();

        if(cmd.isAnnotationPresent(Permission.class))
            this.permission = cmd.getAnnotation(Permission.class).value();

        if(cmd.isAnnotationPresent(NoPermission.class))
            this.noPermissionMsg = cmd.getAnnotation(NoPermission.class).value();

        getGenericClass();
        register();
    }

    @Override
    public final boolean execute(CommandSender commandSender, String s, String[] args) {
        if(this.permission != null && !testPermission(commandSender)) return false;


        if(!parameterClass.isInstance(commandSender)) {
            commandSender.sendMessage(MessageUtils.translateString("&4You cannot use that command."));
            return false;
        }

        execute(parameterClass.cast(commandSender), args);
        return true;
    }

    /**
     * This is fired when the command is executed.
     *
     * @param sender Sender of the command
     * @param args   Command arguments
     */
    public abstract void execute(T sender, String[] args);


    /**
     * This method will register a command without the need of plugin.yml
     */
    public void register() {
        if (_commandMap != null) {
            setFields();
            _commandMap.register(command, this);
            return;
        }

        try {
            Field field = Bukkit.getServer().getClass().getDeclaredField("commandMap");
            field.setAccessible(true);
            _commandMap = (CommandMap) field.get(Bukkit.getServer());

            setFields();

            _commandMap.register(command, this);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setFields() {
        if(this.aliases != null) setAliases(Arrays.asList(this.aliases));
        if(this.description != null) setDescription(this.description);
        if(this.permission != null) setPermission(this.permission);
        if(this.noPermissionMsg != null) setPermissionMessage(this.noPermissionMsg);
        if(this.suggest != null) setSuggest(this.suggest);
    }

    public String getCommand() {
        return this.command;
    }

    public String getSuggest() {
        return this.suggest;
    }

    public void setSuggest(String suggest) {
        this.suggest = suggest;
    }

    public String[] getArrayAliases() {
        return this.aliases;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    public Class<T> getGenericClass() {
        if(this.parameterClass == null) {
            Type superClass = getClass().getGenericSuperclass();
            Type tType = ((ParameterizedType) superClass).getActualTypeArguments()[0];
            String className = tType.toString().split(" ")[1];
            try {
                this.parameterClass = (Class<T>) Class.forName(className);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        return this.parameterClass;
    }
}
