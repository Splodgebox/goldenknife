package net.aminecraftdev.goldenknife.utils;

import org.bukkit.Bukkit;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Dec-17
 */
public class NMSVersionHandler {

    private static String nmsVersion = null;

    public NMSVersionHandler() {
        if(nmsVersion == null) {
            nmsVersion = Bukkit.getServer().getClass().getPackage().getName();
            nmsVersion = nmsVersion.substring(nmsVersion.lastIndexOf(".") + 1);
        }
    }

    public static String getAPIVersion() {
        return nmsVersion;
    }
}
