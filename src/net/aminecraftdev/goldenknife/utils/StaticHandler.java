package net.aminecraftdev.goldenknife.utils;

import org.bukkit.configuration.ConfigurationSection;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 09-Nov-17
 */
public class StaticHandler {

    public static boolean dropOnGround;
    public static double instantKillChance;
    public static int knifeDurability;

    public static boolean failKillDamageWeapon;
    public static boolean failKillKillAttacker;

    public static ConfigurationSection goldenKnifeItem;

}
