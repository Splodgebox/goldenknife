package net.aminecraftdev.goldenknife.utils.file;

import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 20-Oct-17
 */
public abstract class QFile {

    protected static JavaPlugin PLUGIN;

    private File file;

    public QFile(String path) {
        this(new File(path));
    }

    public QFile(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }

    public QFile create() {
        try {
            this.file.createNewFile();
        } catch (IOException ex) {}

        return this;
    }

    protected void setFile(File file) {
        this.file = file;
    }

    public static void setPlugin(JavaPlugin javaPlugin) {
        PLUGIN = javaPlugin;
    }

}
