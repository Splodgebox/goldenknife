package net.aminecraftdev.goldenknife.utils.file.types;

import net.aminecraftdev.goldenknife.utils.file.FileUtils;
import net.aminecraftdev.goldenknife.utils.file.QFile;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 20-Oct-17
 */
public class YmlQFile extends QFile {

    private static Map<String, YmlQFile> ymlFiles = new HashMap<>();

    private YamlConfiguration yamlConfiguration;
    private boolean saveResource = false;
    private JavaPlugin qPlugin;

    public YmlQFile(String path) {
        super(path);

        reloadFile();
        ymlFiles.put(getFile().getName(), this);
    }

    public YmlQFile(File file) {
        super(file);

        reloadFile();
        ymlFiles.put(getFile().getName(), this);
    }

    public YmlQFile saveResource(boolean bool) {
        saveResource = bool;
        return this;
    }

    public YmlQFile addQPlugin(JavaPlugin javaPlugin) {
        this.qPlugin = javaPlugin;
        return this;
    }

    public YmlQFile createFile() {
        if(getFile().exists()) return this;

        if(this.qPlugin != null) {
            if(this.saveResource) {
                this.qPlugin.saveResource(getFile().getName(), false);
            } else {
                create();
            }
        } else {
            if(this.saveResource) {
                PLUGIN.saveResource(getFile().getName(), false);
            } else {
                create();
            }
        }

        reloadFile();
        return this;
    }

    public void saveFile() {
        FileUtils.saveFile(getFile(), yamlConfiguration);
    }

    public void reloadFile() {
        this.yamlConfiguration = FileUtils.getConf(getFile());
    }

    public void delete() {
        getFile().delete();
    }

    public void rename(File file) {
        FileUtils.moveFile(getFile(), file);
        setFile(file);
        reloadFile();
    }

    public YamlConfiguration getConfig() {
        return yamlConfiguration;
    }

    public static Map<String, YmlQFile> getYmlFiles() {
        return ymlFiles;
    }

}
