package net.aminecraftdev.goldenknife.utils.file;

import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by charl on 07-May-17.
 */
public class FileUtils {

    public static void saveFile(File file, YamlConfiguration configuration) {
        try {
            configuration.save(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static YamlConfiguration getConf(File file) {
        return YamlConfiguration.loadConfiguration(file);
    }

    public static void moveFile(File file, File destination) {
        Path source = Paths.get(file.getPath());

        if(destination.exists()) return;

        try {
            Files.move(source, source.resolveSibling(destination.getName()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
