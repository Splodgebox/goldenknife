package net.aminecraftdev.goldenknife.utils;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by charl on 28-Apr-17.
 */
public class CommandUtils {

    public static final Player strAsPlayer(String s) {
        if(Bukkit.getPlayer(s) == null) return null;

        return Bukkit.getPlayer(s);
    }

    public static final Integer strAsInteger(String s) {
        if(NumberUtils.isStringInteger(s)) return Integer.valueOf(s);

        return null;
    }

    public static final Double strAsDouble(String s) {
        if(NumberUtils.isStringDouble(s)) return Double.valueOf(s);

        return null;
    }

    public static final boolean isIntegerWithinItemStackSize(int i) {
        if(i > 64) return false;
        if(i < 1) return false;

        return true;
    }

    public static final boolean isCommandSenderPlayer(CommandSender commandSender) {
        if(!(commandSender instanceof Player)) return false;

        return true;
    }


    public static Integer getAmount(String amount) {
        if(!amount.startsWith("?")) {
            return CommandUtils.strAsInteger(amount);
        } else {
            String replaced = amount.replace("?", "");

            if(!replaced.contains("-")) {
                Integer tempAmount = CommandUtils.strAsInteger(replaced);

                if(tempAmount == null) return null;

                return RandomUtils.getRandomInt(tempAmount) + 1;
            } else {
                String[] split = replaced.split("-");
                String min = split[0];
                String max = split[1];
                Integer minAmount = CommandUtils.strAsInteger(min);
                Integer maxAmount = CommandUtils.strAsInteger(max);


                if(minAmount == null || maxAmount == null) return null;

                return RandomUtils.getRandomInt(maxAmount) + minAmount;
            }
        }
    }
}