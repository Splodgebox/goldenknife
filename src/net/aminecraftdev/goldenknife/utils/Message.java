package net.aminecraftdev.goldenknife.utils;

import net.aminecraftdev.goldenknife.utils.message.MessageUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.text.SimpleDateFormat;

/**
 * Created by charl on 21-Apr-17.
 */
public enum Message {

    DATE_FORMAT("date-format", "MM/d/yy h:ma"), // 3/31/15 07:49AM
    NO_PERMISSION("NoPermission", "&c&l** &cYou do not have permission to that! &c&l**"),
    INVALID_INTEGER("InvalidInteger", "&c&l** &cYou specified an invalid number! &c&l**"),
    INVENTORY_SPACE("InventorySpace", "&c&l** &cYou do not have enough inventory space! &c&l**"),
    NOT_ONLINE("NotOnline", "&c&l** &cThe specified player is offline! &c&l**"),

    COMMAND_GOLDENKNIFE_INVALIDARGS("&c&l** &cInvalid args! &nUse /goldenknife (player) (amount)&c&l **"),
    COMMAND_GOLDENKNIFE_RELOADED("&e&l** &bYou have reloaded the YML files! &e&l**"),
    COMMAND_GOLDENKNIFE_RECEIVED_SENDER("&e&l** &bYou have given &n{0} {1}x&b knives. &e&l**"),
    COMMAND_GOLDENKNIFE_RECEIVED_RECEIVER("&e&l** &bYou have received &n{0}x&b knives. &e&l**"),
    COMMAND_GOLDENKNIFE_DROPPEDTOGROUND("&c&oYour inventory was full so some were dropped to the ground!"),

    GOLDENKNIFE_KILLED(
            "&7\n" +
            "{c}&6{0}&e has slain &6{1}\n" +
            "{c}&6using &e&l&nThe Golden Knife&r\n" +
            "&7"),
    GOLDENKNIFE_CANNOTDAMAGE("&c&l** &cYou cannot damage that with this weapon! &c&l**"),
    GOLDENKNIFE_FAILEDCHANCE("&c&l** &cYou weren't successful in using the Golden Knife **"),
    GOLDENKNIFE_KILLEDSELF("&cThe Golden Knife had such a negative effect, the effects were reversed and you killed yourself!");


    private String path;
    private String msg;
    private static FileConfiguration LANG;
    public static SimpleDateFormat sdf;

    Message(String path, String start) {
        this.path = path;
        this.msg = start;
    }

    Message(String string) {
        this.path = this.name().replace("_", ".");
        this.msg = string;
    }

    public static void setFile(FileConfiguration configuration) {
        LANG = configuration;
        sdf = new SimpleDateFormat(DATE_FORMAT.toString());
    }

    @Override
    public String toString() {
        return ChatColor.translateAlternateColorCodes('&', LANG.getString(this.path, msg));
    }

    public String getDefault() {
        return this.msg;
    }

    public String getPath() {
        return this.path;
    }

    public void msg(CommandSender p, Object... args) {
        String s = toString();

        if(s.contains("\n")) {
            String[] split = s.split("\n");

            for(String inner : split) {
                sendMessage(p, inner, args);
            }

        } else {
            sendMessage(p, s, args);
        }
    }

    public void broadcast(Object... args) {
        String s = toString();

        if(s.contains("\n")) {
            String[] split = s.split("\n");

            for(String inner : split) {
                for(Player player : Bukkit.getOnlinePlayers()) {
                    sendMessage(player, inner, args);
                }
            }
        } else {
            for(Player player : Bukkit.getOnlinePlayers()) {
                sendMessage(player, s, args);
            }
        }
    }

    private String getFinalized(String string, Object... order) {
        int current = 0;

        for(Object object : order) {
            String placeholder = "{" + current + "}";

            if(string.contains(placeholder)) {
                if(object instanceof CommandSender) {
                    string = string.replace(placeholder, ((CommandSender) object).getName());
                }
                else if(object instanceof OfflinePlayer) {
                    string = string.replace(placeholder, ((OfflinePlayer) object).getName());
                }
                else if(object instanceof Location) {
                    Location location = (Location) object;
                    String repl = location.getWorld().getName() + ", " + location.getBlockX() + ", " + location.getBlockY() + ", " + location.getBlockZ();

                    string = string.replace(placeholder, repl);
                }
                else if(object instanceof String) {
                    string = string.replace(placeholder, MessageUtils.translateString((String) object));
                }
                else if(object instanceof Double) {
                    string = string.replace(placeholder, ""+object);
                }
                else if(object instanceof Integer) {
                    string = string.replace(placeholder, ""+object);
                }
                else if(object instanceof ItemStack) {
                    string = string.replace(placeholder, getItemStackName((ItemStack) object));
                }
            }

            current++;
        }

        return string;
    }

    private void sendMessage(CommandSender target, String string, Object... order) {
        string = getFinalized(string, order);

        if(string.contains("{c}")) {
            MessageUtils.sendCenteredMessage(target, string.replace("{c}", ""));
        } else {
            target.sendMessage(string);
        }
    }

    private String getItemStackName(ItemStack itemStack) {
        String name = itemStack.getType().toString().replace("_", " ");

        if(itemStack.hasItemMeta() && itemStack.getItemMeta().hasDisplayName()) {
            return itemStack.getItemMeta().getDisplayName();
        }

        return name;
    }
}
