package net.aminecraftdev.goldenknife.commands;

import net.aminecraftdev.goldenknife.GoldenKnife;
import net.aminecraftdev.goldenknife.utils.CommandUtils;
import net.aminecraftdev.goldenknife.utils.Message;
import net.aminecraftdev.goldenknife.utils.StaticHandler;
import net.aminecraftdev.goldenknife.utils.command.CommandService;
import net.aminecraftdev.goldenknife.utils.command.attributes.Alias;
import net.aminecraftdev.goldenknife.utils.command.attributes.Description;
import net.aminecraftdev.goldenknife.utils.command.attributes.Name;
import net.aminecraftdev.goldenknife.utils.command.attributes.Permission;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@Name("goldenknife")
@Alias({"gk", "goldenknives", "goldenk"})
@Description("Used to obtain the Golden Knife and reload the plugin")
@Permission("gk.cmd.use")
public class GoldenKnifeCmd extends CommandService<CommandSender> {

    private GoldenKnife PLUGIN = GoldenKnife.getI();

    public GoldenKnifeCmd() {
        super(GoldenKnifeCmd.class);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if(args.length == 0) {
            Message.COMMAND_GOLDENKNIFE_INVALIDARGS.msg(commandSender);
            return;
        }

        if (args.length == 1 && args[0].equalsIgnoreCase("reload")) {
            PLUGIN.reloadFiles();
            Message.COMMAND_GOLDENKNIFE_RELOADED.msg(commandSender);
            return;
        }

        if (args.length != 2) {
            Message.COMMAND_GOLDENKNIFE_INVALIDARGS.msg(commandSender);
            return;
        }

        Player player = CommandUtils.strAsPlayer(args[0]);
        Integer amount = CommandUtils.getAmount(args[1]);

        if (player == null) {
            Message.NOT_ONLINE.msg(commandSender);
            return;
        }

        if (amount == null) {
            Message.INVALID_INTEGER.msg(commandSender);
            return;
        }

        if ((player.getInventory().firstEmpty() == -1) && (!StaticHandler.dropOnGround)) {
            Message.INVENTORY_SPACE.msg(commandSender);
            return;
        }

        boolean wasAnyDropped = false;

        for (int i = 1; i <= amount; i++) {
            if ((player.getInventory().firstEmpty() == -1) && (StaticHandler.dropOnGround)) {
                if (!wasAnyDropped) wasAnyDropped = true;

                player.getWorld().dropItemNaturally(player.getLocation(), PLUGIN.getGoldenKnife());
            } else {
                player.getInventory().addItem(PLUGIN.getGoldenKnife());
            }
        }

        Message.COMMAND_GOLDENKNIFE_RECEIVED_SENDER.msg(commandSender, player.getName(), amount);
        Message.COMMAND_GOLDENKNIFE_RECEIVED_RECEIVER.msg(player, amount);

        if (wasAnyDropped) Message.COMMAND_GOLDENKNIFE_DROPPEDTOGROUND.msg(player);
    }
}
