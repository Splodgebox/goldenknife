package net.aminecraftdev.goldenknife.listeners;

import net.aminecraftdev.goldenknife.GoldenKnife;
import net.aminecraftdev.goldenknife.utils.Message;
import net.aminecraftdev.goldenknife.utils.PluginUtils;
import net.aminecraftdev.goldenknife.utils.RandomUtils;
import net.aminecraftdev.goldenknife.utils.StaticHandler;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by charl on 21-Apr-17.
 */
public class KnifeHitListener implements Listener {

    private static GoldenKnife PLUGIN = GoldenKnife.getI();

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPvP(EntityDamageByEntityEvent event) {
        if(event.isCancelled()) return;
        if(!(event.getDamager() instanceof Player)) return;
        Player damager = (Player) event.getDamager();
        ItemStack itemStack = PluginUtils.getItemInHand(damager);

        if(PLUGIN.isNotGoldenKnife(itemStack)) return;

        int durability = PLUGIN.getDurability(itemStack);

        if(durability >= StaticHandler.knifeDurability) {
            clearHand(damager);
            event.setDamage(0);
            return;
        }

        if(!(event.getEntity() instanceof Player)) {
            event.setCancelled(true);
            Message.GOLDENKNIFE_CANNOTDAMAGE.msg(damager);
            return;
        }

        if(StaticHandler.instantKillChance < 100) {
            int randomNumber = RandomUtils.getRandomInt(100) + 1;

            if(randomNumber > StaticHandler.instantKillChance) {
                if(StaticHandler.failKillDamageWeapon) {
                    event.setDamage(0);
                } else {
                    event.setCancelled(true);
                }

                Message.GOLDENKNIFE_FAILEDCHANCE.msg(damager);

                if(StaticHandler.failKillKillAttacker) {
                    Message.GOLDENKNIFE_KILLEDSELF.msg(damager);
                    damager.setHealth(0.0);
                }

                return;
            }
        }

        Player damaged = (Player) event.getEntity();

        event.setCancelled(true);
        damaged.setHealth(0.0D);
        Message.GOLDENKNIFE_KILLED.broadcast(damager.getName(), damaged.getName());

        if(durability + 1 >= StaticHandler.knifeDurability) {
            clearHand(damager);
        } else {
            int newDurability = durability+1;

            PluginUtils.setItemInHand(damager, PLUGIN.setDurability(itemStack, newDurability));
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockBreak(BlockBreakEvent event) {
        if(event.isCancelled()) return;
        Player player = event.getPlayer();
        ItemStack itemStack = PluginUtils.getItemInHand(player);

        if(itemStack == null || itemStack.getType() == Material.AIR) return;
        if(PLUGIN.isNotGoldenKnife(itemStack)) return;

        event.setCancelled(true);
        Message.GOLDENKNIFE_CANNOTDAMAGE.msg(player);
    }

    private void clearHand(Player player) {
        PluginUtils.setItemInHand(player, new ItemStack(Material.AIR));
    }
}
